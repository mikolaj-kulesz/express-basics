const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");
const error404Routes = require("./routes/404");
const app = express();

app.set("view engine", "ejs");
app.set("views", "views");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use("/", (req, res, next) => {
  console.log("this always runs...");
  next();
});

app.use("/admin", adminRoutes.router);
app.use(shopRoutes);
app.use(error404Routes);

app.listen(3000);
