const Product = require('../models/product.js');
const Cart = require('../models/cart.js');

exports.getProducts = async (req, res, next) => {
    const [products] = await Product.fetchAll();
    res.render('shop/product-list', {
        pageTitle: 'Shop Product List',
        path: '/products',
        prods: products,
    });
};

exports.getProductDetails = async (req, res, next) => {
    const {params} = req
    const {productId} = params
    const products = await Product.fetchAll();
    const detailProduct = products.find(prod => prod.id === productId)
    res.render('shop/product-detail', {
        pageTitle: `Product Details - ${productId}`,
        path: `/products`,
        product: detailProduct,
    });
};

exports.getIndex = async (req, res, next) => {
    const [products] = await Product.fetchAll();
    res.render('shop/index', {
        pageTitle: 'Shop Index Page',
        path: '/',
        prods: products,
    });
};

exports.getCart = async (req, res, next) => {
    const {products} = await Cart.fetchAll();
    const allProducts = await Product.fetchAll();
    const cartProducts = products.map(cp => {
        return {
            ...allProducts.find(p => p.id === cp.id),
            ...cp
        }
    });
    const cartTotal = cartProducts.reduce((total, item) => {
        return total + (+item.price * item.qty)
    }, 0).toFixed(2);

    res.render('shop/cart', {
        pageTitle: 'Shop Cart',
        path: '/cart',
        total: cartTotal,
        products: cartProducts,
    });
};

exports.addToCart = async (req, res, next) => {
    const {productId} = req.body;
    await Cart.add(productId);
    res.redirect("/cart");
}

exports.removeFromCart = async (req, res, next) => {
    const {productId} = req.params;
    await Cart.delete(productId)
    res.redirect("/cart");
}

exports.getOrders = async (req, res, next) => {
    const products = await Product.fetchAll();
    res.render('shop/orders', {
        pageTitle: 'Orders',
        path: '/orders',
        prods: products,
    });
};

exports.getCheckout = async (req, res, next) => {
    const products = await Product.fetchAll();
    res.render('shop/checkout', {
        pageTitle: 'Shop Checkout',
        path: '/checkout',
        prods: products,
    });
};
