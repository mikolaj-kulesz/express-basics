const Product = require("../models/product.js");
const Cart = require('../models/cart.js');

// GET
exports.getAddProduct = (req, res, next) => {
    res.render("admin/edit-product", {
        pageTitle: "Add product",
        path: "/admin/add-product",
        edit: false,
        product: {},
    });
}

exports.getEditProduct = async (req, res, next) => {
    const {productId} = req.params
    const {edit} = req.query
    const products = await Product.fetchAll();
    const product = products.find(p => p.id === productId);
    if (!product) return res.redirect("/");
    res.render("admin/edit-product", {
        pageTitle: "edit product",
        path: "/admin/edit-product",
        edit,
        product,
    });
}

exports.getProducts = async (req, res, next) => {
    const products = await Product.fetchAll();
    res.render('admin/products', {
        pageTitle: 'Admin Products',
        path: '/admin/products',
        prods: products,
    });
}

// POST
exports.postAddProduct = async (req, res, next) => {
    const product = new Product(req.body);
    await product.save();
    res.redirect("/");
}

exports.postEditProduct = async (req, res, next) => {
    const {productId} = req.params
    const productDetails = req.body
    await Product.edit(productId, productDetails);
    res.redirect(`/admin/products`);
}

exports.deleteProduct = async (req, res, next) => {
    const {productId} = req.params
    const {products: cartProducts} = await Cart.fetchAll();
    const isProductAddedToTheCart = cartProducts.find(p => p.id === productId);
    if(isProductAddedToTheCart) await Cart.delete(productId);
    await Product.delete(productId);
    res.redirect(`/admin/products`);
}
