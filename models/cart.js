const fs = require("fs").promises;
const path = require("path");
const basePath = require("../utils/paths")
const p = path.join(basePath, "data", "cart.json");
const Product = require("./product.js")

const getCart = async () => {
    let data
    try {
        data = await fs.readFile(p, "binary");
        return JSON.parse(data);
    } catch {
        return {
            products: [],
        };
    }
}

const getProductById = async (id) => {
    const products = await Product.fetchAll();
    return products.find(p => p.id === id);
}
 
module.exports = class Cart {

    static async add(productId){
        const product = await getProductById(productId);
        const {products: cartProducts} = await Cart.fetchAll();
        const isProductAdded = cartProducts.find(p => p.id === productId);
        const newCartProducts = isProductAdded
            ? cartProducts.map(p => p.id === productId ? {...p, qty: p.qty + 1} : p)
            : [...cartProducts, {
                id: productId,
                qty: 1,
            }];
        const newCart = {
            products: newCartProducts,
        }
        try {
            fs.writeFile(p, JSON.stringify(newCart));
        } catch {
            console.error("Something is wrong with saving files!");
        }
    }

    static async delete(productId){
        const {products: cartProducts} = await Cart.fetchAll();
        const {price} = await getProductById(productId);
        const {qty} = cartProducts.find(p => p.id === productId);
        const newSetOfProducts = cartProducts.filter(p => p.id !== productId);
        const newCart = {
            products: newSetOfProducts,
        }
        try {
            fs.writeFile(p, JSON.stringify(newCart));
        } catch {
            console.error("Something is wrong with saving files!");
        }
    }

    static async fetchAll(){
        return await getCart();
    }
} 
