const fs = require("fs").promises;
const db = require("../utils/database");
const path = require("path");
const basePath = require("../utils/paths")
const p = path.join(basePath, "data", "products.json");
const getProducts = async () => {
    let data
    try {
        data = await fs.readFile(p, "binary");
        return JSON.parse(data);
    } catch {
        return [];
    }
}


module.exports = class Product {
    constructor({title, description, price, imageUrl}) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
        this.id = Math.random().toString();
    }

    async save(){
        const products = await getProducts();
        products.push(this);
        try {
            fs.writeFile(p, JSON.stringify(products));
        } catch {
            console.error("Something is wrong with saving files!");
        }
    }

    static async edit(productId, newProps){
        const products = await getProducts();
        const editedSetOfProducts = products.map(p => p.id === productId ? {...p, ...newProps} : p)
        try {
            fs.writeFile(p, JSON.stringify(editedSetOfProducts));
        } catch {
            console.error("Something is wrong with saving files!");
        }
    }

    static async delete(productId){
        const products = await getProducts();
        const newSetOfProducts = products.filter(p => p.id !== productId)
        try {
            fs.writeFile(p, JSON.stringify(newSetOfProducts));
        } catch {
            console.error("Something is wrong with saving files!");
        }
    }

    static fetchAll(){
        // return await getProducts();
        // const db = require("./utils/database");
        return db.execute('SELECT * FROM products');
    }
} 
