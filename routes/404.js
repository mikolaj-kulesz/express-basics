const express = require("express");
const router = express.Router();
const errorController = require("../controllers/error.js")

router.use("/", errorController.get404);

module.exports = router;
